FROM alpine

ENV TERM=screen-256color
ENV LANG=C.UTF-8
RUN apk add --no-cache dumb-init py3-pip tmux weechat-python weechat-perl weechat; pip3 install websocket-client
VOLUME /weechat
EXPOSE 9001
USER 1000:1000
ENTRYPOINT ["/usr/bin/dumb-init", "/run.sh"]
ADD run.sh /
